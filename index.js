const fs = require("node:fs");
const fsPromises = require("node:fs/promises");

const { FROM, TO } = process.env;

if (!FROM || !TO) {
  console.log(`No FROM or TO`);
  process.exit(1);
}

console.log(`Copying from ${FROM} to ${TO}`);

fs.watchFile(FROM, async (curr, prev) => {
  const data = await fsPromises.readFile(FROM);
  await fsPromises.writeFile(TO, data);
  console.log(`Copied`);
});
